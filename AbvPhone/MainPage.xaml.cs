﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using AbvPhone.Resources;
using Microsoft.Phone.Tasks;
using System.IO;
using System.Text;
using System.Net.Http;

namespace AbvPhone
{
	public partial class MainPage : PhoneApplicationPage
	{
		CameraCaptureTask cameraCaptureTask;
		PhotoChooserTask photoChooserTask;
		// Constructor
		public MainPage()
		{
            ///comment
			InitializeComponent();
			
			
			cameraCaptureTask = new CameraCaptureTask();
			cameraCaptureTask.Completed += new EventHandler<PhotoResult>(cameraCaptureTask_Completed);


			// Sample code to localize the ApplicationBar
			photoChooserTask = new PhotoChooserTask();
			photoChooserTask.Completed += new EventHandler<PhotoResult>(photoChooserTask_Completed);
			//BuildLocalizedApplicationBar();
		}
		void cameraCaptureTask_Completed(object sender, PhotoResult e)
		{
			if (e.TaskResult == TaskResult.OK)
			{
				MessageBox.Show(e.ChosenPhoto.Length.ToString());
			}
		}
		private void Button_Click(object sender, RoutedEventArgs e)
		{
			//cameraCaptureTask.Show();
			photoChooserTask.Show();
			
		}

		void photoChooserTask_Completed(object sender, PhotoResult e)
		{
			if (e.TaskResult == TaskResult.OK)
			{
				var photoResult = e as PhotoResult;
				MemoryStream memoryStream = new MemoryStream();
				photoResult.ChosenPhoto.CopyTo(memoryStream);
				byte[] myPicArray = memoryStream.ToArray();
				//string result = Convert.ToBase64String(myPicArray);
				string result = "Hello";

				using (var client = new System.Net.Http.HttpClient())
				{
					// New code:
					//client.BaseAddress = new Uri("http://192.168.8.102:9001");
					client.DefaultRequestHeaders.Accept.Clear();
					//client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

					//var message = new Message() { MessageId = 0, FromId = Globals.MemberId, ToId = memberId, Content = text, Timestamp = DateTime.Now };
					//var json_object = JsonConvert.SerializeObject(message);

					var postTask = client.GetStringAsync("http://192.168.8.102:9001/api/image/" + result); //, new StringContent(result, Encoding.UTF8, "application/json")
					//var urlContents = await postTask;
					
				}



				//WebRequest request = WebRequest.Create("http://localhost:9001/api/image/" + result);
				////request.Credentials = CredentialCache.DefaultCredentials;
				//request.Method = "POST";
				//request.ContentLength = result.Length;
				//request.ContentType = "application/x-www-form-urlencoded";
				//Stream dataStream = request.GetRequestStream();
				//dataStream.Write(byteArray, 0, byteArray.Length);
				//dataStream.Close();
				//WebResponse response = request.GetResponse();



				//((HttpWebRequest)request).UserAgent = ".NET Framework Example Client";
				//WebClient webClient = new WebClient();
				//webClient.OpenReadCompleted += new OpenReadCompletedEventHandler(wc_OpenReadCompleted);
				//Uri u = new Uri();
				////Stream stream = await webClient.OpenReadTaskAsync(new Uri("http://externalURl.com/sample.xml", UriKind.Absolute)); 
				////webClient.OpenWriteCompleted += new OpenWriteCompletedEventHandler(wc_OpenWriteCompleted);
				//webClient.OpenReadAsync(u);
				//webClient.OpenWriteAsync(u, "GET", myPicArray);

			
			}
		}
		public static void wc_OpenReadCompleted(object sender, OpenReadCompletedEventArgs e)
		{
			if (e.Error == null)
			{
				
				string sendData = e.UserState as string;
				object[] objArr = e.UserState as object[];
				byte[] fileContent = e.UserState as byte[];

				Stream outputStream = e.Result;
				outputStream.Write(fileContent, 0, fileContent.Length);
				outputStream.Flush();
				outputStream.Close();
				string s = e.Result.ToString(); ;

			}
		}
		public static void wc_OpenWriteCompleted(object sender, OpenWriteCompletedEventArgs e)
		{
			if (e.Error == null)
			{
				string sendData = e.UserState as string;
				object[] objArr = e.UserState as object[];
				byte[] fileContent = e.UserState as byte[];

				Stream outputStream = e.Result;
				outputStream.Write(fileContent, 0, fileContent.Length);
				outputStream.Flush();
				outputStream.Close();
				string s = e.Result.ToString(); ;

			}
		}

		// Sample code for building a localized ApplicationBar
		//private void BuildLocalizedApplicationBar()
		//{
		//    // Set the page's ApplicationBar to a new instance of ApplicationBar.
		//    ApplicationBar = new ApplicationBar();

		//    // Create a new button and set the text value to the localized string from AppResources.
		//    ApplicationBarIconButton appBarButton = new ApplicationBarIconButton(new Uri("/Assets/AppBar/appbar.add.rest.png", UriKind.Relative));
		//    appBarButton.Text = AppResources.AppBarButtonText;
		//    ApplicationBar.Buttons.Add(appBarButton);

		//    // Create a new menu item with the localized string from AppResources.
		//    ApplicationBarMenuItem appBarMenuItem = new ApplicationBarMenuItem(AppResources.AppBarMenuItemText);
		//    ApplicationBar.MenuItems.Add(appBarMenuItem);
		//}
	}
}